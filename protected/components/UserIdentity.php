<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    private $id;

    public function authenticate() {
        $username = strtolower($this->username); //Aquí pongo en minúscula
        $user = Usuarios::model()->find('Login=?', array($username)); //minuscula
        if ($user === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if (!$user->validatePassword($this->password))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->id = $user->id;
            $this->username = $user->Login;
            ///$this->setState('roles', $user->roles);
            $this->setState('login', $user->Login);
            $this->setState('nombre', $user->Nombre);
            $this->setState('rol', $user->rol);
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode == self::ERROR_NONE;
    }

    public function getId() {
        return $this->id;
    }

}
