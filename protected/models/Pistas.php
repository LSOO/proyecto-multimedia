<?php

/**
 * This is the model class for table "pistas".
 *
 * The followings are the available columns in table 'pistas':
 * @property integer $id
 * @property string $Nombre
 * @property string $Ruta
 * @property integer $usuarios_id
 * @property integer $PerfilUsuario_id
 *
 * The followings are the available model relations:
 * @property Detallespistacompartida[] $detallespistacompartidas
 * @property Pistacompartida[] $pistacompartidas
 * @property Usuarios $usuarios
 * @property Perfilusuario $perfilUsuario
 */
class Pistas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pistas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, Ruta, usuarios_id, PerfilUsuario_id', 'required'),
			array('usuarios_id, PerfilUsuario_id', 'numerical', 'integerOnly'=>true),
			array('Nombre', 'length', 'max'=>45),
                        array('Ruta','length','max'=>200),
                    
                        
                        array('Ruta','file', 'types' => 'mp3', 'allowEmpty' => true, 'maxSize' => 1024 * 1024 * 10, 'tooLarge' => 'La pista pesa mas de 10MB. Porfavor seleccione un archivo mas pequeño.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Nombre, Ruta, usuarios_id, PerfilUsuario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detallespistacompartidas' => array(self::HAS_MANY, 'Detallespistacompartida', 'Pistas_id_u_compartido'),
			'pistacompartidas' => array(self::HAS_MANY, 'Pistacompartida', 'Pistas_id'),
			'usuarios' => array(self::BELONGS_TO, 'Usuarios', 'usuarios_id'),
			'perfilUsuario' => array(self::BELONGS_TO, 'Perfilusuario', 'PerfilUsuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Nombre' => 'Nombre',
			'Ruta' => 'Ruta',
			'usuarios_id' => 'Usuarios',
			'PerfilUsuario_id' => 'Perfil',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Ruta',$this->Ruta,true);
		$criteria->compare('usuarios_id',$this->usuarios_id);
		$criteria->compare('PerfilUsuario_id',$this->PerfilUsuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pistas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
