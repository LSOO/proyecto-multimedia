<?php

/**
 * This is the model class for table "detallespistacompartida".
 *
 * The followings are the available columns in table 'detallespistacompartida':
 * @property integer $id
 * @property integer $usuarios_id
 * @property integer $Perfiles_id
 * @property integer $PistaCompartida_id
 * @property integer $Pistas_id_u_compartido
 * @property string $mensaje
 *
 * The followings are the available model relations:
 * @property Usuarios $usuarios
 * @property Perfiles $perfiles
 * @property Pistacompartida $pistaCompartida
 * @property Pistas $pistasIdUCompartido
 */
class Detallespistacompartida extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detallespistacompartida';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuarios_id, Perfiles_id, PistaCompartida_id', 'required'),
			array('usuarios_id, Perfiles_id, PistaCompartida_id, Pistas_id_u_compartido', 'numerical', 'integerOnly'=>true),
			array('mensaje', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, usuarios_id, Perfiles_id, PistaCompartida_id, Pistas_id_u_compartido, mensaje', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarios' => array(self::BELONGS_TO, 'Usuarios', 'usuarios_id'),
			'perfiles' => array(self::BELONGS_TO, 'Perfiles', 'Perfiles_id'),
			'pistaCompartida' => array(self::BELONGS_TO, 'Pistacompartida', 'PistaCompartida_id'),
			'pistasIdUCompartido' => array(self::BELONGS_TO, 'Pistas', 'Pistas_id_u_compartido'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'usuarios_id' => 'Usuarios',
			'Perfiles_id' => 'Perfiles',
			'PistaCompartida_id' => 'Pista Compartida',
			'Pistas_id_u_compartido' => 'Pistas Id U Compartido',
			'mensaje' => 'Mensaje',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('usuarios_id',$this->usuarios_id);
		$criteria->compare('Perfiles_id',$this->Perfiles_id);
		$criteria->compare('PistaCompartida_id',$this->PistaCompartida_id);
		$criteria->compare('Pistas_id_u_compartido',$this->Pistas_id_u_compartido);
		$criteria->compare('mensaje',$this->mensaje,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Detallespistacompartida the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
