<?php

/**
 * This is the model class for table "usuarios".
 *
 * The followings are the available columns in table 'usuarios':
 * @property integer $id
 * @property string $Login
 * @property string $Nombre
 * @property string $Apellido
 * @property string $Password
 * @property double $Puntuacion
 * @property string $email
 * @property string $rol
 * @property string $usuarioscol
 *
 * The followings are the available model relations:
 * @property Detallespistacompartida[] $detallespistacompartidas
 * @property Perfilusuario[] $perfilusuarios
 * @property Pistacompartida[] $pistacompartidas
 * @property Pistas[] $pistases
 * @property Seguidos[] $seguidoses
 * @property Seguidos[] $seguidoses1
 */
class Usuarios extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'usuarios';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Login, Nombre, Apellido, Password, email, rol', 'required'),
            array('Puntuacion', 'numerical'),
            array('Login, Password, usuarioscol', 'length', 'max' => 45),
            array('Nombre, Apellido, email', 'length', 'max' => 60),
            array('rol', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, Login, Nombre, Apellido, Password, Puntuacion, email, rol, usuarioscol', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'detallespistacompartidas' => array(self::HAS_MANY, 'Detallespistacompartida', 'usuarios_id'),
            'perfilusuarios' => array(self::HAS_MANY, 'Perfilusuario', 'usuarios_id'),
            'pistacompartidas' => array(self::HAS_MANY, 'Pistacompartida', 'usuarios_id'),
            'pistases' => array(self::HAS_MANY, 'Pistas', 'usuarios_id'),
            'seguidoses' => array(self::HAS_MANY, 'Seguidos', 'seguidor'),
            'seguidoses1' => array(self::HAS_MANY, 'Seguidos', 'seguido'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'Login' => 'Login',
            'Nombre' => 'Nombre',
            'Apellido' => 'Apellido',
            'Password' => 'Password',
            'Puntuacion' => 'Puntuacion',
            'email' => 'Email',
            'rol' => 'Rol',
            'usuarioscol' => 'Usuarioscol',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('Login', $this->Login, true);
        $criteria->compare('Nombre', $this->Nombre, true);
        $criteria->compare('Apellido', $this->Apellido, true);
        $criteria->compare('Password', $this->Password, true);
        $criteria->compare('Puntuacion', $this->Puntuacion);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('rol', $this->rol, true);
        $criteria->compare('usuarioscol', $this->usuarioscol, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usuarios the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function validatePassword($clave) {
        return $this->hashPassword($clave) === $this->Password;
    }

    public function hashPassword($clave) {
        return $clave;
    }

}
