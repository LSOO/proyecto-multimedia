<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */

$this->breadcrumbs=array(
	'Instrumentos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Instrumento', 'url'=>array('index')),
	array('label'=>'Manage Instrumento', 'url'=>array('admin')),
);
?>

<h1>Create Instrumento</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>