<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */

$this->breadcrumbs=array(
	'Instrumentos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Instrumento', 'url'=>array('index')),
	array('label'=>'Create Instrumento', 'url'=>array('create')),
	array('label'=>'View Instrumento', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Instrumento', 'url'=>array('admin')),
);
?>

<h1>Update Instrumento <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>