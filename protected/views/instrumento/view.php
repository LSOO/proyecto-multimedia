<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */

$this->breadcrumbs=array(
	'Instrumentos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Instrumento', 'url'=>array('index')),
	array('label'=>'Create Instrumento', 'url'=>array('create')),
	array('label'=>'Update Instrumento', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Instrumento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Instrumento', 'url'=>array('admin')),
);
?>

<h1>View Instrumento #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'Nombre',
	),
)); ?>
