<?php
/* @var $this InstrumentoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Instrumentos',
);

$this->menu=array(
	array('label'=>'Create Instrumento', 'url'=>array('create')),
	array('label'=>'Manage Instrumento', 'url'=>array('admin')),
);
?>

<h1>Instrumentos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
