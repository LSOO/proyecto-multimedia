<?php
/* @var $this PerfilusuarioController */
/* @var $model Perfilusuario */

$this->breadcrumbs=array(
	'Perfilusuarios'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Perfilusuario', 'url'=>array('index')),
	array('label'=>'Manage Perfilusuario', 'url'=>array('admin')),
);
?>

<h1>Create Perfilusuario</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>