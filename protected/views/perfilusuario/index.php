<?php
/* @var $this PerfilusuarioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Perfilusuarios',
);

$this->menu=array(
	array('label'=>'Create Perfilusuario', 'url'=>array('create')),
	array('label'=>'Manage Perfilusuario', 'url'=>array('admin')),
);
?>

<h1>Perfilusuarios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
