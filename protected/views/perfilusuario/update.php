<?php
/* @var $this PerfilusuarioController */
/* @var $model Perfilusuario */

$this->breadcrumbs=array(
	'Perfilusuarios'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Perfilusuario', 'url'=>array('index')),
	array('label'=>'Create Perfilusuario', 'url'=>array('create')),
	array('label'=>'View Perfilusuario', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Perfilusuario', 'url'=>array('admin')),
);
?>

<h1>Update Perfilusuario <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>