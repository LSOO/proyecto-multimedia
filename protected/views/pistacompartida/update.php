<?php
/* @var $this PistacompartidaController */
/* @var $model Pistacompartida */

$this->breadcrumbs=array(
	'Pistacompartidas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pistacompartida', 'url'=>array('index')),
	array('label'=>'Create Pistacompartida', 'url'=>array('create')),
	array('label'=>'View Pistacompartida', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Pistacompartida', 'url'=>array('admin')),
);
?>

<h1>Update Pistacompartida <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>