<?php
/* @var $this PistacompartidaController */
/* @var $model Pistacompartida */

$this->breadcrumbs=array(
	'Pistacompartidas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pistacompartida', 'url'=>array('index')),
	array('label'=>'Manage Pistacompartida', 'url'=>array('admin')),
);
?>

<h1>Create Pistacompartida</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>