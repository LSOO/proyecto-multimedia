<?php
/* @var $this PistacompartidaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pistacompartidas',
);

$this->menu=array(
	array('label'=>'Create Pistacompartida', 'url'=>array('create')),
	array('label'=>'Manage Pistacompartida', 'url'=>array('admin')),
);
?>

<h1>Pistacompartidas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
