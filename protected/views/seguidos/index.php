<?php
/* @var $this SeguidosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Seguidoses',
);

$this->menu=array(
	array('label'=>'Create Seguidos', 'url'=>array('create')),
	array('label'=>'Manage Seguidos', 'url'=>array('admin')),
);
?>

<h1>Seguidoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
