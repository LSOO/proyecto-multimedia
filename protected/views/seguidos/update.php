<?php
/* @var $this SeguidosController */
/* @var $model Seguidos */

$this->breadcrumbs=array(
	'Seguidoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Seguidos', 'url'=>array('index')),
	array('label'=>'Create Seguidos', 'url'=>array('create')),
	array('label'=>'View Seguidos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Seguidos', 'url'=>array('admin')),
);
?>

<h1>Update Seguidos <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>