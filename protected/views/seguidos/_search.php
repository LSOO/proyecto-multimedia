<?php
/* @var $this SeguidosController */
/* @var $model Seguidos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seguidor'); ?>
		<?php echo $form->textField($model,'seguidor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seguido'); ?>
		<?php echo $form->textField($model,'seguido'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->