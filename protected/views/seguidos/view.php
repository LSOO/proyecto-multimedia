<?php
/* @var $this SeguidosController */
/* @var $model Seguidos */

$this->breadcrumbs=array(
	'Seguidoses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Seguidos', 'url'=>array('index')),
	array('label'=>'Create Seguidos', 'url'=>array('create')),
	array('label'=>'Update Seguidos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Seguidos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Seguidos', 'url'=>array('admin')),
);
?>

<h1>View Seguidos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'seguidor',
		'seguido',
	),
)); ?>
