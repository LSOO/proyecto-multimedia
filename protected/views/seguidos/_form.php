<?php
/* @var $this SeguidosController */
/* @var $model Seguidos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'seguidos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'seguidor'); ?>
		<?php echo $form->textField($model,'seguidor'); ?>
		<?php echo $form->error($model,'seguidor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'seguido'); ?>
		<?php echo $form->textField($model,'seguido'); ?>
		<?php echo $form->error($model,'seguido'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->