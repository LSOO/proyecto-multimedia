<?php
/* @var $this SeguidosController */
/* @var $model Seguidos */

$this->breadcrumbs=array(
	'Seguidoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Seguidos', 'url'=>array('index')),
	array('label'=>'Manage Seguidos', 'url'=>array('admin')),
);
?>

<h1>Create Seguidos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>