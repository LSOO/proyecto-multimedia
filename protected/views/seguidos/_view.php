<?php
/* @var $this SeguidosController */
/* @var $data Seguidos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seguidor')); ?>:</b>
	<?php echo CHtml::encode($data->seguidor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seguido')); ?>:</b>
	<?php echo CHtml::encode($data->seguido); ?>
	<br />


</div>