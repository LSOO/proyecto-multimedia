
<script type="text/javascript"><?php
echo $pujs;
?>



</script>

<div class="tabbable-panel">
    <div class="tabbable-line">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#perfiluser" data-toggle="tab">
                    Mi Perfil </a>
            </li>

            <?php
            if ($valido == true) {
                echo '<li>
                <a href="#subir" data-toggle="tab">
                    Subir Nueva Pista </a>
                 </li>';
            }
            ?>

        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="perfiluser">


                <!-- Perfil de usuario -->
                <div class="well profile">
                    <div class="col-sm-12">
                        <div class="col-xs-12 col-sm-8">
                            <h2><?php echo $model->Nombre . ' ' . $model->Apellido ?></h2>
                            <p><strong>Acerca de mi: </strong> Web Designer / UI. </p>
                            <p><strong>Hobbies: </strong> Read, out with friends, listen to music, draw and learn new things. </p>
                            <p><strong>Perfiles: </strong>
                                <?php
                                foreach ($perfiles as $perfil) {
                                    echo '<span class="tags">';
                                    echo $perfil->perfiles->Nombre;
                                    echo '</span>';
                                }
                                ?>

                            </p>
                        </div>             
                        <div class="col-xs-12 col-sm-4 text-center">
                            <figure>
                                <img src="http://www.localcrimenews.com/wp-content/uploads/2013/07/default-user-icon-profile.png" alt="" class="img-circle img-responsive">
                                <figcaption class="ratings">
                                    <p>Puntuacion
                                        <a href="#">
                                            <span class="fa fa-star"></span>
                                        </a>
                                        <a href="#">
                                            <span class="fa fa-star"></span>
                                        </a>
                                        <a href="#">
                                            <span class="fa fa-star"></span>
                                        </a>
                                        <a href="#">
                                            <span class="fa fa-star"></span>
                                        </a>
                                        <a href="#">
                                            <span class="fa fa-star-o"></span>
                                        </a> 
                                    </p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>      

                    <div class="col-xs-12 divider text-center">
                        <div class="col-xs-12 col-sm-4 emphasis">
                            <h2><strong> <?php echo $siguidores ?></strong></h2>                    
                            <p><small>Seguidores</small></p>
                            <!--<button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span> Seguir</button> -->
                        </div>
                        <div class="col-xs-12 col-sm-4 emphasis">
                            <h2><strong><?php echo $siguiendo ?></strong></h2>                    
                            <p><small>Siguiendo</small></p>
                            <!--<button class="btn btn-info btn-block"><span class="fa fa-user"></span> View Profile </button> -->  
                        </div>
                        <div class="col-xs-12 col-sm-4 emphasis">
                            <h2><strong><?php echo count($pistas) ?></strong></h2>                    
                            <p><small>Pistas</small></p>
                            <!--
                            <div class="btn-group dropup btn-block">
                              <button type="button" class="btn btn-primary"><span class="fa fa-gear"></span> Options </button>
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu text-left" role="menu">
                                <li><a href="#"><span class="fa fa-envelope pull-right"></span> Send an email </a></li>
                                <li><a href="#"><span class="fa fa-list pull-right"></span> Add or remove from a list  </a></li>
                                <li class="divider"></li>
                                <li><a href="#"><span class="fa fa-warning pull-right"></span>Report this user for spam</a></li>
                                <li class="divider"></li>
                                <li><a href="#" class="btn disabled" role="button"> Unfollow </a></li>
                              </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- Fin perfil -->

                <!--- Pistas del usuario -->
                <h3>Pistas</h3>



                <!--
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Perfil X</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>
                -->
                <div class="tabbable-panel">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs ">
                            <?php
                            $con = 0;
                            foreach ($arraypistas as $key => $value) {
                                if ($con == 0) {
                                    echo '<li class="active">';
                                    $con++;
                                } else {
                                    echo '<li>';
                                }
                                echo '<a href="#' . $key . '" data-toggle="tab">' . $key . '</a>';
                                echo '</li>';
                            }
                            ?>

                        </ul>
                        <div class="tab-content">
                            <?php
                            $con = 0;
                            foreach ($arraypistas as $key => $value) {
                                if ($con == 0) {
                                    echo '<div class="tab-pane active" id="' . $key . '">';
                                    $con++;
                                } else {
                                    echo '<div class="tab-pane" id="' . $key . '">';
                                }

                                foreach ($value as $tem) {
                                    echo '<div id="pista">';
                                    echo 'Pista:  ' . $tem->Nombre;

                                    /// Waveform de la pista
                                    echo '<div id="waveform' . $tem->id . '">';


                                    /// Fin del div de waveform
                                    echo '</div>';
                                    echo '<div id="wave-timeline' . $tem->id . '"></div>';


                                    echo '<div class="btn-group">';
                                    /// Botonoes para Cargar
                                    echo '<button class="btn btn-primary" id="bt' . $tem->id . '" onclick="Cargar(' . $tem->id . ',' . "'$tem->Ruta'" . ')">';
                                    echo '<i class="glyphicon glyphicon-upload"></i>';
                                    echo 'Cargar </button>';

                                    /// Boton para reproducir
                                    echo '<button class="btn btn-primary" id="btp' . $tem->id . '" onclick="PlayUser(' . $tem->id . ')" disabled>';
                                    echo '<i class="glyphicon glyphicon-play"></i>';


                                    echo 'Play</button>';
                                    echo '</div>';

                                    /// Fin del div pista
                                    echo '</div>';
                                }
                                echo '</div>';
                            }
                            ?>
                        </div>





                        <!--
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                ASDASDASD
                            </div>
                            <div class="tab-pane" id="tab2">
                                <p>
                                    tab2.
                                </p>
                            </div>
                        </div>
                        -->
                    </div>
                </div>
            </div>










            <div class="tab-pane" id="subir">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'pistas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => false,
                    'clientOptions' => array('validateOnSubmit' => false),
                    'htmlOptions' => array('enctype' => 'multipart/form-data')
                ));
                ?>

                <p class="note">Campos con <span class="required">*</span> son requeridos.</p>

                <?php //echo $form->errorSummary($modelpista);    ?>
                <fieldset>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelpista, 'Nombre'); ?>
                        <?php echo $form->textField($modelpista, 'Nombre', array('size' => 45, 'maxlength' => 45, 'class' => 'form-control')); ?>
                        <?php echo $form->error($modelpista, 'Nombre', array('class' => 'alert alert-danger', 'role' => 'alert')); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelpista, 'Ruta'); ?>
                        <?php echo $form->fileField($modelpista, 'Ruta', array('size' => 45, 'maxlength' => 45, 'class' => 'form-control')); ?>
                        <?php echo $form->error($modelpista, 'Ruta', array('class' => 'alert alert-danger', 'role' => 'alert')); ?>
                    </div>


                    <div class="form-group">
                        <?php echo $form->labelEx($modelpista, 'PerfilUsuario_id'); ?>
                        <?php echo $form->dropDownList($modelpista, 'PerfilUsuario_id', $arrayperfiles,array('empty' => 'Seleccione un perfil','class' => 'form-control')); ?>
                        <?php echo $form->error($modelpista, 'PerfilUsuario_id', array('class' => 'alert alert-danger', 'role' => 'alert')); ?>
                    </div>
                </fieldset>
                <div class="form-group">
                    <?php echo CHtml::submitButton('Subir', array('class' => 'btn btn-lg btn-success btn-block')); ?>
                </div>

                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<br><br>
