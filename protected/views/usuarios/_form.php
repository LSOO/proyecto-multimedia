<?php
/* @var $this UsuariosController */
/* @var $model Usuarios */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuarios-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con<span class="required">*</span> son requiridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Login'); ?>
		<?php echo $form->textField($model,'Login',array('size'=>45,'maxlength'=>45,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'Login'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Nombre'); ?>
		<?php echo $form->textField($model,'Nombre',array('size'=>60,'maxlength'=>60,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'Nombre'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Apellido'); ?>
		<?php echo $form->textField($model,'Apellido',array('size'=>60,'maxlength'=>60,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'Apellido'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Password'); ?>
		<?php echo $form->passwordField($model,'Password',array('size'=>45,'maxlength'=>45,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'Password'); ?>
	</div>

	

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>60,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

        <?php if(Yii::app()->user->rol == "admin")
        { ?>
        <div class="form-group">
		<?php echo $form->labelEx($model,'Puntuacion'); ?>
		<?php echo $form->textField($model,'Puntuacion',array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'Puntuacion'); ?>
	</div>
        
	<div class="form-group">
		<?php echo $form->labelEx($model,'rol'); ?>
		<?php echo $form->textField($model,'rol',array('size'=>10,'maxlength'=>10,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'rol'); ?>
	</div>
        <?php 
        }
        ?>  

	<div class="form-group">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->