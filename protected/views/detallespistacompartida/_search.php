<?php
/* @var $this DetallespistacompartidaController */
/* @var $model Detallespistacompartida */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuarios_id'); ?>
		<?php echo $form->textField($model,'usuarios_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Perfiles_id'); ?>
		<?php echo $form->textField($model,'Perfiles_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PistaCompartida_id'); ?>
		<?php echo $form->textField($model,'PistaCompartida_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Pistas_id_u_compartido'); ?>
		<?php echo $form->textField($model,'Pistas_id_u_compartido'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mensaje'); ?>
		<?php echo $form->textArea($model,'mensaje',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->