<?php
/* @var $this DetallespistacompartidaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Detallespistacompartidas',
);

$this->menu=array(
	array('label'=>'Create Detallespistacompartida', 'url'=>array('create')),
	array('label'=>'Manage Detallespistacompartida', 'url'=>array('admin')),
);
?>

<h1>Detallespistacompartidas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
