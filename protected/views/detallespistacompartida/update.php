<?php
/* @var $this DetallespistacompartidaController */
/* @var $model Detallespistacompartida */

$this->breadcrumbs=array(
	'Detallespistacompartidas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Detallespistacompartida', 'url'=>array('index')),
	array('label'=>'Create Detallespistacompartida', 'url'=>array('create')),
	array('label'=>'View Detallespistacompartida', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Detallespistacompartida', 'url'=>array('admin')),
);
?>

<h1>Update Detallespistacompartida <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>