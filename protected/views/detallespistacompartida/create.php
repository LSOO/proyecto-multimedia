<?php
/* @var $this DetallespistacompartidaController */
/* @var $model Detallespistacompartida */

$this->breadcrumbs=array(
	'Detallespistacompartidas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Detallespistacompartida', 'url'=>array('index')),
	array('label'=>'Manage Detallespistacompartida', 'url'=>array('admin')),
);
?>

<h1>Create Detallespistacompartida</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>