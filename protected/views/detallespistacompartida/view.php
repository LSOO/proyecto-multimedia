<?php
/* @var $this DetallespistacompartidaController */
/* @var $model Detallespistacompartida */

$this->breadcrumbs=array(
	'Detallespistacompartidas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Detallespistacompartida', 'url'=>array('index')),
	array('label'=>'Create Detallespistacompartida', 'url'=>array('create')),
	array('label'=>'Update Detallespistacompartida', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Detallespistacompartida', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Detallespistacompartida', 'url'=>array('admin')),
);
?>

<h1>View Detallespistacompartida #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'usuarios_id',
		'Perfiles_id',
		'PistaCompartida_id',
		'Pistas_id_u_compartido',
		'mensaje',
	),
)); ?>
