<?php
/* @var $this DetallespistacompartidaController */
/* @var $data Detallespistacompartida */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuarios_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuarios_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Perfiles_id')); ?>:</b>
	<?php echo CHtml::encode($data->Perfiles_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PistaCompartida_id')); ?>:</b>
	<?php echo CHtml::encode($data->PistaCompartida_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Pistas_id_u_compartido')); ?>:</b>
	<?php echo CHtml::encode($data->Pistas_id_u_compartido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mensaje')); ?>:</b>
	<?php echo CHtml::encode($data->mensaje); ?>
	<br />


</div>