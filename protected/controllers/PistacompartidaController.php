<?php

class PistacompartidaController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'index', 'view'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        ///Array de pistas que se le va a mandar a la vista
        $detpistas = [];

        ///Ruta canciones
        $rpistas = [];

        ///Cargo el modelo de la pista compartida
        $model = $this->loadModel($id);

        $detpistas['Original']['id'] = $model->Pistas_id;
        $detpistas['Original']['creador'] = $model->pistas->usuarios->Nombre;
        $detpistas['Original']['ruta'] = $model->pistas->Ruta;
        $detpistas['Original']['creadorid'] = $model->usuarios_id;

        $rpistas[$model->Pistas_id] = $model->pistas->Ruta;
        
        
        
        
        $detpistaco = Detallespistacompartida::model()->findAllByAttributes(array('PistaCompartida_id' => $id));

        //$perfilesusuario = Perfilusuario::model()->find AllByAttributes(array('usuarios_id'=>3));
        //var_dump($perfilesusuario);
        
        
        $con = 0;
        foreach ($detpistaco as $valor) {
            if ($valor['Pistas_id_u_compartido'] != null) {
                $arrytem['creador'] = Usuarios::model()->findByPk($valor['usuarios_id'])->Nombre;
                $arrytem['creadorid'] = $valor['usuarios_id'];
                $arrytem['id'] = $valor['Pistas_id_u_compartido'];
                $tem = Pistas::model()->findByPk($valor['Pistas_id_u_compartido']);
                $arrytem['ruta'] = $tem->Ruta;
                
                
                $rpistas[$valor['Pistas_id_u_compartido']] = $tem->Ruta;
                $detpistas[$valor->perfiles->Nombre]["$con"] = $arrytem;

                $con++;
            }
        }
        
        $this->render('view', array('detpistas' => $detpistas,'rpistas'=>$this->event($rpistas)));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Pistacompartida;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Pistacompartida'])) {
            $model->attributes = $_POST['Pistacompartida'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Pistacompartida'])) {
            $model->attributes = $_POST['Pistacompartida'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Pistacompartida');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Pistacompartida('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Pistacompartida']))
            $model->attributes = $_GET['Pistacompartida'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Pistacompartida the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Pistacompartida::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Pistacompartida $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pistacompartida-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function event($arraycanciones) 
    {

        $variable = "var wavesurfer =[];\n";

        foreach ($arraycanciones as $posicion => $ruta) {

            $variable = $variable . "wavesurfer[" . $posicion . "] = Object.create(WaveSurfer);\n";

            $variable = $variable . "document.addEventListener('DOMContentLoaded', function () {\n";
            $variable = $variable . "var id='" . $posicion . "';\n";
            $variable = $variable . "var ruta='" . $ruta . "';\n";
            $variable = $variable . "Waveform(id,ruta,wavesurfer[" . $posicion . "]);\n";
            $variable = $variable . "},false);\n";
        }
        return $variable;
    }

}
