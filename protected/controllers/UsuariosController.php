<?php

class UsuariosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {

        $modelpistas = new Pistas();
        
        /// Perfiles Usuario
        $arrayp = [];
        /// Saco todos los perfiles que tiene el usuarios
        $perfiles = Perfilusuario::model()->findAllByAttributes(array('usuarios_id' => $id));

        /// Array pistas con id el perfil (Un array donde en cada el key es el Instrumento y el contenido son las pistas)
        $arraypistas = [];
        foreach ($perfiles as $value) {
            $key = $value->perfiles->Nombre;
            $arrayp["$value->id"]=$value->perfiles->Nombre;
            $arraypistas[$key] = Pistas::model()->findAllByAttributes(array('usuarios_id' => $id, 'PerfilUsuario_id' => $value->id));
        }

        //Saca todas las pistas que tiene el usuario
        $tempistas = Pistas::model()->findAllByAttributes(array('usuarios_id' => $id));

        $pistasusuario = [];
        foreach ($tempistas as $pista) {
            $pistasusuario["$pista->id"] = $pista->Ruta;
        }



        $siguiendo = Seguidos::model()->findAllByAttributes(array('seguidor' => $id));
        $siguidores = Seguidos::model()->findAllByAttributes(array('seguido' => $id));
        $pistas = Pistas::model()->findAllByAttributes(array('usuarios_id' => $id));


        $valido = Yii::app()->user->id == $id;


        if (isset($_POST['Pistas'])) {
            $modelpistas->attributes = $_POST['Pistas'];
            $modelpistas->Ruta = CUploadedFile::getInstance($modelpistas, 'Ruta');
            if ($modelpistas->validate()) {


                $path = 'C:\\xampp\\htdocs\\proyecto-multimedia\\media\\' . Yii::app()->user->id . $modelpistas->Ruta;
                var_dump($path);
                $modelpistas->Ruta->saveAs($path);
                $modelpistas->Ruta = '/proyecto-multimedia/media/' . Yii::app()->user->id . $modelpistas->Ruta;
                $modelpistas->usuarios_id = Yii::app()->user->id;
                $modelpistas->save();
                $modelpistas = new Pistas();
            }
        }



        // para ver cosas del perfil usuario conectado = usario q esta viendo

        $this->render('view', array(
            'model' => $this->loadModel($id),
            'perfiles' => $perfiles,
            'siguiendo' => count($siguiendo),
            'siguidores' => count($siguidores),
            'pistas' => $pistas,
            'modelpista' => $modelpistas,
            'arraypistas' => $arraypistas,
            'pujs' => $this->event($pistasusuario),
            'valido' => $valido,
            'arrayperfiles'=>$arrayp,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Usuarios;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Usuarios'])) {
            $model->attributes = $_POST['Usuarios'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Usuarios'])) {
            $model->attributes = $_POST['Usuarios'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Usuarios('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Usuarios']))
            $model->attributes = $_GET['Usuarios'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Usuarios the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Usuarios::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Usuarios $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuarios-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function event($arraycanciones) {

        $variable = "var wavesurfer =[];\n";

        foreach ($arraycanciones as $posicion => $ruta) {

            $variable = $variable . "wavesurfer[" . $posicion . "] = Object.create(WaveSurfer);\n";

            $variable = $variable . "document.addEventListener('DOMContentLoaded', function () {\n";
            $variable = $variable . "var id='" . $posicion . "';\n";
            $variable = $variable . "var ruta='" . $ruta . "';\n";
            $variable = $variable . "Waveform2(id,ruta,wavesurfer[" . $posicion . "]);\n";
            $variable = $variable . "},false);\n";
        }
        return $variable;
    }

}
