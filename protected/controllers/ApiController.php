<?php
header('Access-Control-Allow-Origin: *'); 
class ApiController extends Controller
{

 
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array();
    }
 
    // Actions
    /// Esta solo la debe hacer el desarrolador
    public function actionDetallepitasc()
    {

        /// Para verifar que la peticion venga con un id
        if(!isset($_GET['id']))
        {
            $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );
        }
        
        ///Array de los ids de las canciones y su rua
        $id=$_GET['id'];
        /// Saca el modelo que correspondo al ese id
        $canciones = [];
        $model = Pistacompartida::model()->findByPk($id);
        
        //var_dump($model->pistas->Ruta);
        
        if (!empty($model))
        {
            $canciones[$model->Pistas_id] = $model->pistas->Ruta;
        }
        
        /// Detalles de la pista compartida
        $detalles = Detallespistacompartida::model()->findAllByAttributes(array('PistaCompartida_id'=>$id));
        
        //var_dump($detalles);
        
        if(!empty($detalles))
        {
            foreach($detalles as $individual)
            {
                $canciones[$individual->pistasIdUCompartido->id] = $individual->pistasIdUCompartido->Ruta;
                //$canciones[] = $individual->id;
                
            }
        }
        $this->_sendResponse(200, CJSON::encode($canciones));
        
        //var_dump($model);
        
        /// Para saber que modelo llega por GET
        /*
        if($_GET['id'])
        {
                $this->_sendResponse(200, "Esta Todo Perfecto" );
        }
        else
        {
            $this->_sendResponse(501, sprintf('Error: Modo <b>Listado</b> No esta implementado para el modelo <b>%s</b>', $_GET['model']) );
                Yii::app()->end();
        }*/

    }

    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }



    /// Para la respuesta del usuario
    private function _sendResponse($status = 200, $body, $content_type = 'text/html')
    {
        
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        
                 
        echo $body;
        
        Yii::app()->end();
    }
    


}




?>