<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>UTrack</title>

        <link href="data:image/gif;" rel="icon" type="image/x-icon" />

        <!-- Bootstrap -->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

        <link rel="stylesheet" href="../css/style.css" />
        <link rel="stylesheet" href="../css/ribbon.css" />

        <link rel="screenshot" itemprop="screenshot" href="http://katspaugh.github.io/wavesurfer.js/example/screenshot.png" />

        <!-- wavesurfer.js -->
        <script src="build/wavesurfer.min.js"></script>

        <!-- timeline format renderer -->
        <script src="plugin/wavesurfer.timeline.js"></script>

        <!-- App -->
        <script src="app.js"></script>
        <script src="trivia.js"></script>
    </head>

    <body itemscope itemtype="http://schema.org/WebApplication">
        <div class="container">
            <div class="header">
                <h1 itemprop="name">UTrack-Canciones Compartidas</h1>
            </div>

            <div id="demo">
                Cancion 1
                <div id="waveform">
                    <div class="progress progress-striped active" id="progress-bar">
                        <div class="progress-bar progress-bar-info"></div>
                    </div>

                    <!-- Here be waveform -->
                </div>
                <div id="wave-timeline"></div>
            </br></br>

                Cancion 2
                <div id="waveform1">
                    <div class="progress progress-striped active" id="progress-bar1">
                        <div class="progress-bar1 progress-bar-info"></div>
                    </div>

                    <!-- Here be waveform -->
                </div>
                <div id="wave-timeline1"></div>


               

                <div class="controls">
                    <button class="btn btn-primary" onclick="play();">
                        <i class="glyphicon glyphicon-play"></i>
                        Play
                        /
                        <i class="glyphicon glyphicon-pause"></i>
                        Pause
                    </button>
                </div>
            </div>



            <div class="footer row">
                <div class="col-sm-12">
                    <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/80x15.png" /></a>
                </div>

                <div class="col-sm-12">
                    <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">wavesurfer.js</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/katspaugh/wavesurfer.js" property="cc:attributionName" rel="cc:attributionURL">katspaugh</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.en_US">Creative Commons Attribution 3.0 Unported License</a>.
                </div>
            </div>
        </div>

        <div class="github-fork-ribbon-wrapper right">
            <div class="github-fork-ribbon">
                <a itemprop="isBasedOnUrl" href="https://github.com/katspaugh/wavesurfer.js">Fork me on GitHub</a>
            </div>
        </div>

        <script>
            var wavesurfer = Object.create(WaveSurfer);
document.addEventListener('DOMContentLoaded', function () {
    var id='';
    var ruta='../media/demo.wav'
    Waveform(id,ruta,wavesurfer);
},false);






// Create an instance
var wavesurfer1 = Object.create(WaveSurfer);
document.addEventListener('DOMContentLoaded', function () {
    var id='1';
    var ruta='../media/cancion.mp3'
    Waveform(id,ruta,wavesurfer1);
},false);
        </script>
    </body>
</html>
