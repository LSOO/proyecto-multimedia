'use strict';

// Create an instance




function Waveform(id, ruta, wavesurfer)
{
    var options = {
        container: '#waveform' + id,
        waveColor: 'violet',
        progressColor: 'purple',
        loaderColor: 'purple',
        cursorColor: 'navy'
    };



    wavesurfer.on('ready', function () {
        // Init Timeline plugin
        var timeline = Object.create(WaveSurfer.Timeline);

        timeline.init({
            wavesurfer: wavesurfer,
            container: '#wave-timeline' + id
        });

    });

    // Init wavesurfer
    wavesurfer.init(options);
    wavesurfer.load(ruta);
}
var primera = true;
var actuales = [];
/// Funcion para validad los checkbox
var ids = [];
var canciones = [];
function validate()
{
    var checkBoxes = document.getElementsByTagName('input');
    //alert(checkBoxes.length);

    var param = "Values of Checkboxes:" + "\n";
    for (var counter = 0; counter < checkBoxes.length; counter++)
    {
        if (checkBoxes[counter].type.toUpperCase() == 'CHECKBOX' && checkBoxes[counter].checked == true)
        {
            param += checkBoxes[counter].value + "\n";
            ids.push(parseInt(checkBoxes[counter].value));

        }
    }


}




function play() {
    validate();
    if (primera == false)
    {
        for (var k in actuales) {
            wavesurfer[actuales[k]].stop();

        }
        primera = true;
        actuales = [];
        ids = [];
    }
    else
    {

        if (ids.length != 0)
        {
            for (var k in ids)
            {
                actuales[k] = ids[k];
                wavesurfer[ids[k]].playPause();

            }
            primera = false;
        }
    }

}

/// Funcuones para la vista de usuario
function Waveform2(id, ruta, wavesurfer)
{
    var options = {
        container: '#waveform' + id,
        waveColor: 'violet',
        progressColor: 'purple',
        loaderColor: 'purple',
        cursorColor: 'navy'
    };

    // Progress bar
    // Progress bar


    wavesurfer.on('ready', function () {
        // Init Timeline plugin
        var timeline = Object.create(WaveSurfer.Timeline);

        timeline.init({
            wavesurfer: wavesurfer,
            container: '#wave-timeline' + id
        });

    });

    // Init wavesurfer
    wavesurfer.init(options);
    //wavesurfer.load(ruta);
}

function Cargar(id,ruta) {
    wavesurfer[id].on('load', wavesurfer[id].play.bind(wavesurfer)); wavesurfer[id].load(ruta); 
    document.getElementById("bt"+id).disabled = true;
    document.getElementById("btp"+id).removeAttribute("disabled");

    }
    
function PlayUser(id)
{
    wavesurfer[id].playPause();
}