<!DOCTYPE html>
<!-- saved from url=(0054)http://getbootstrap.com/examples/sticky-footer-navbar/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="http://getbootstrap.com/favicon.ico">

        <title><?php echo Yii::app()->name ?></title>

        <!-- Bootstrap core CSS -->
        <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

        <link href="http://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css" rel="stylesheet">
        
        
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        
        <!-- Librerias para el waveform -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/build/wavesurfer.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugin/wavesurfer.timeline.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/app.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/trivia.js"></script>
        <!-- -->

        <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>

        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Lato:400,700);
            body
            {
                font-family: 'Lato', 'sans-serif';
            }
            .profile 
            {
                min-height: 355px;
                display: inline-block;
            }
            figcaption.ratings
            {
                margin-top:20px;
            }
            figcaption.ratings a
            {
                color:#f1c40f;
                font-size:11px;
            }
            figcaption.ratings a:hover
            {
                color:#f39c12;
                text-decoration:none;
            }
            .divider 
            {
                border-top:1px solid rgba(0,0,0,0.1);
            }
            .emphasis 
            {
                border-top: 4px solid transparent;
            }
            .emphasis:hover 
            {
                border-top: 4px solid #1abc9c;
            }
            .emphasis h2
            {
                margin-bottom:0;
            }
            span.tags 
            {
                background: #1abc9c;
                border-radius: 2px;
                color: #f5f5f5;
                font-weight: bold;
                padding: 2px 4px;
            }
            .dropdown-menu 
            {
                background-color: #34495e;    
                box-shadow: none;
                -webkit-box-shadow: none;
                width: 250px;
                margin-left: -125px;
                left: 50%;
            }
            .dropdown-menu .divider 
            {
                background:none;    
            }
            .dropdown-menu>li>a
            {
                color:#f5f5f5;
            }
            .dropup .dropdown-menu 
            {
                margin-bottom:10px;
            }
            .dropup .dropdown-menu:before 
            {
                content: "";
                border-top: 10px solid #34495e;
                border-right: 10px solid transparent;
                border-left: 10px solid transparent;
                position: absolute;
                bottom: -10px;
                left: 50%;
                margin-left: -10px;
                z-index: 10;
            }
        </style>




    </head>

    <body>




        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">

            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex-collapse">
                        <span class="sr-only">Expand the menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">UTrack</a>
                </div>
                <div class="collapse navbar-collapse navbar-ex-collapse">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'encodeLabel' => false,
                        'items' => array(
                            array('label' => '<span class="glyphicon glyphicon-home"></span>Mi Perfil', 'url' => array('/usuarios/' . Yii::app()->user->id)),
                            array('label' => '<span class="glyphicon glyphicon-list-alt"></span>Mis Pistas', 'url' => array('/site/page', 'view' => 'about')),
                        //array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                        ),
                        'activeCssClass' => 'active',
                        'activateParents' => true,
                        'htmlOptions' => array('class' => 'nav navbar-nav'),
                    ));
                    ?>


                    <form class="navbar-form navbar-left" role="search" action=<?php echo Yii::app()->createUrl('personas/buscar') ?> method="get">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Buscar Artistas" name="q" value="<?= isset($_GET['q']) ? CHtml::encode($_GET['q']) : ''; ?>">
                        </div>
                        <button type="submit" class="btn btn-default">Buscar</button>
                    </form>




                    <!-- Menu de la parte derecha -->
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-music"></span>Invitaciones <span class="label label-info">1</span>
                                <ul class="dropdown-menu">
                                    <li><a href="#"><span class="label label-warning">Luis Santiago</span>Pista con Guitarra</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" class="text-center">Ver Todas</a></li>
                                </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                                    class="glyphicon glyphicon-user"></span><?php echo Yii::app()->user->nombre ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><?php echo CHtml::link(CHtml::openTag('span class="glyphicon glyphicon-user"') . CHtml::closeTag('span') . 'Perfil') ?></li>

                                <li class="divider"></li>
                                <li><?php echo CHtml::link(CHtml::openTag('span class="glyphicon glyphicon-off"') . CHtml::closeTag('span') . 'Salir', array('site/logout')) ?></li>
                            </ul>
                        </li>
                    </ul>


                </div>

            </div>
        </nav>

        <!-- Begin page content -->


        <div class="container" >
            <div class="row">

                <div class="col-md-8 col-md-offset-2">
                    <?php echo $content; ?>
                </div>

            </div>



        </div>





        <footer class="footer">
            <div class="container">
                <p class="text-muted" align='center'>UTrack. Red Social de Musica</p>
            </div>
        </footer>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie10-viewport-bug-workaround.js"></script>

        <link rel="stylesheet"  href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css">


    </body></html>