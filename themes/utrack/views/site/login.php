    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Login</h2>
                </div>
                <div class="panel-body">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'login-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                    ));
                    ?>

                    <fieldset>
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'username'); ?>
                            <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'username', array('class' => 'alert alert-danger', 'role' => 'alert')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'password'); ?>
                            <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'password', array('class' => 'alert alert-danger', 'role' => 'alert')); ?>

                        </div>
                        <div class="checkbox">
                            <label>
                                <?php echo $form->checkBox($model, 'rememberMe'); ?>
                                Recordarme
                            </label>
                            
                            
                        </div>
                        
                        
                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">

                    </fieldset>
                    <div class="or-box row-block">
                    <div class="row">
                        <div class="col-md-12 row-block">
                            <a href="#" class="btn btn-primary btn-block">Crear Cuenta nueva</a>
                        </div>
                    </div>
                </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>